# create_mdadm_array

Create a Linux software RAID array via Ansible.

----

#### Usage:

Include the role in your playbook, then either use a preset vars file in /vars, or pass in a `mdadm_arrays` dictionary at runtime. See `vars/i3.4xlarge` for an example.

###### Example:
Where your playbook includes the role, and you wish to use a template file.
```
ansible-playbook --extra-vars 'instance_size_disk_template=i3.4xlarge raid_level=0' -i "somehostname," main.yml
```
