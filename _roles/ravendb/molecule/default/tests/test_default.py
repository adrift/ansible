import os
import json

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


def raven_service(host):
    service = host.service("ravendb")

    assert service.is_enabled
    assert service.is_running


def ravendb_config(host):
    file = host.file("/opt/RavenDB/settings.json")

    assert file.exists
    ravendb_config = json.loads(file)

    assert '"DataDir"' in ravendb_config
    assert '"Setup.Mode"' in ravendb_config
