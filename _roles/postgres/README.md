# postgres via ansible

This tries to enable postgres (10) in a very friendly "vanilla" way. It'll be up to your related specific playbooks to ingest this, and customize further.

### quick-start

After adding the role to your playbook
```
ansible-playbook -i "machine-hostname," -u ubuntu main.yml
```

### customization

The full PostgreSQL configuration has been converted into easily accessible default variables that you can override for your use-case whether that be prototyping or production testing. Nearly all values are identically named, but have a `pg_` prefix to them so as to not conflict with other potential vars.

See `defaults/main.yml` and `templates/postgresql.conf.j2` for the full list.
