param (
    [String]
    $rust_dir = "E:\gs\rust\Server",
    
    [String]
    $terminal_process = "cmd"
)

function Stop-RustServer {
    try {
        Stop-Service -Name "rust"
    }
    catch {
        Write-Error "Error during stopping process: $_.Exception.Message"
    }
}

function Start-RustServer {
    Start-Service -Name "rust"
}

Set-Location $rust_dir

########
# Stop existing server
########
if ((Get-Service -Name Rust).Status -eq "Running") {
    Write-Output "Stopping existing Rust server"
    Stop-RustServer
}
else {
    Write-Error "No server processes found for Rust, continuing!"
}

########
# Update server
########
try {
    Write-Output "Updating Rust now!"
    & "$rust_dir\update.bat"
}
catch {
    Write-Error "Error during update process: $_.Exception.Message"
}

########
# Start server
########
Start-RustServer